<?php
/******************
***PHPBOT
***Author:wangwei
***Email:915335526@qq.com
***Versions:1.0
******************/
header('Content-Type:text/html;charset=utf-8');//编码设置

include 'path.php';

if(isset($_FILES['upfile'])){
	fileUp();
}else{
	$prefix = '""';
	echo '{"imageActionName":"uploadimage","imageFieldName":"upfile","imageMaxSize":2048000,"imageAllowFiles":[".png",".jpg",".jpeg",".gif",".bmp"],"imageCompressEnable":true,"imageCompressBorder":1600,"imageInsertAlign":"none","imageUrlPrefix":'.$prefix.',"imagePathFormat":"upload/image/{yyyy}{mm}{dd}/{time}{rand:6}","scrawlActionName":"uploadscrawl","scrawlFieldName":"upfile","scrawlPathFormat":"upload/image/{yyyy}{mm}{dd}/{time}{rand:6}","scrawlMaxSize":2048000,"scrawlUrlPrefix":'.$prefix.',"scrawlInsertAlign":"none","snapscreenActionName":"uploadimage","snapscreenPathFormat":"upload/image/{yyyy}{mm}{dd}/{time}{rand:6}","snapscreenUrlPrefix":'.$prefix.',"snapscreenInsertAlign":"none","catcherLocalDomain":["127.0.0.1","localhost","img.baidu.com"],"catcherActionName":"catchimage","catcherFieldName":"source","catcherPathFormat":"upload/image/{yyyy}{mm}{dd}/{time}{rand:6}","catcherUrlPrefix":'.$prefix.',"catcherMaxSize":2048000,"catcherAllowFiles":[".png",".jpg",".jpeg",".gif",".bmp"],"videoActionName":"uploadvideo","videoFieldName":"upfile","videoPathFormat":"upload/video/{yyyy}{mm}{dd}/{time}{rand:6}","videoUrlPrefix":'.$prefix.',"videoMaxSize":102400000,"videoAllowFiles":[".flv",".swf",".mkv",".avi",".rm",".rmvb",".mpeg",".mpg",".ogg",".ogv",".mov",".wmv",".mp4",".webm",".mp3",".wav",".mid"],"fileActionName":"uploadfile","fileFieldName":"upfile","filePathFormat":"upload/file/{yyyy}{mm}{dd}/{time}{rand:6}","fileUrlPrefix":'.$prefix.',"fileMaxSize":51200000,"fileAllowFiles":[".png",".jpg",".jpeg",".gif",".bmp",".flv",".swf",".mkv",".avi",".rm",".rmvb",".mpeg",".mpg",".ogg",".ogv",".mov",".wmv",".mp4",".webm",".mp3",".wav",".mid",".rar",".zip",".tar",".gz",".7z",".bz2",".cab",".iso",".doc",".docx",".xls",".xlsx",".ppt",".pptx",".pdf",".txt",".md",".xml"],"imageManagerActionName":"listimage","imageManagerListPath":"upload/image","imageManagerListSize":20,"imageManagerUrlPrefix":'.$prefix.',"imageManagerInsertAlign":"none","imageManagerAllowFiles":[".png",".jpg",".jpeg",".gif",".bmp"],"fileManagerActionName":"listfile","fileManagerListPath":"upload/file","fileManagerUrlPrefix":'.$prefix.',"fileManagerListSize":20,"fileManagerAllowFiles":[".png",".jpg",".jpeg",".gif",".bmp",".flv",".swf",".mkv",".avi",".rm",".rmvb",".mpeg",".mpg",".ogg",".ogv",".mov",".wmv",".mp4",".webm",".mp3",".wav",".mid",".rar",".zip",".tar",".gz",".7z",".bz2",".cab",".iso",".doc",".docx",".xls",".xlsx",".ppt",".pptx",".pdf",".txt",".md",".xml"]
}';

}

function creaDir($dirPath){
	 $curPath = dirname(__FILE__);
	 $path = $curPath.'/'.$dirPath;

	 if (is_dir($path) || mkdir($path,0777,true)) {
	  return $dirPath;
	 }
}

function fileUp($src=null){
	//判断文件是否为空或者出错
	$upFile = $_FILES['upfile'];
		
	if ($upFile['error']==0 && !empty($upFile)) {

		if($upFile['type'] != 'image/jpg' && $upFile['type'] != 'image/jpeg' && $upFile['type'] != 'image/bmp' && $upFile['type'] != 'image/png' && $upFile['type'] != 'image/gif' && $upFile['type'] != 'image/pjpeg' && $upFile['type'] != 'image/x-png'){echo '格式错误';}
		
		$filename = date("Y").date("m").date("d").date("H").date("i").date("s").rand(100, 999).".".substr(strrchr($upFile['name'],"."),1);
		
		$cunurl = '';
		foreach($GLOBALS['tempmenu'] as $kt =>$vt){
			$dirpath = creaDir('../../'.$vt.'/file/'.($src?$src:'').'/'.date('Y-m-d'));
			$queryPath = $dirpath.'/'.$filename;
			if(is_file($upFile['tmp_name'])){
				if(!move_uploaded_file($upFile['tmp_name'],$queryPath)){echo '移动失败';};
				$cunurl = $queryPath;
			}else{
				copy($cunurl,$queryPath);
			}
		}
		echo '{"url":"../file/'.date('Y-m-d').'/'.$filename.'","title":"'.$filename.'","state":"SUCCESS"}';
	}
	else{
		echo '文件为空';
	}
}
	
?>