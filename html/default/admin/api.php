<?php
/******************
***PHPBOT
***Author:wangwei
***Email:915335526@qq.com
***Versions:1.0
******************/
header('Content-Type:text/html;charset=utf-8');//编码设置

include 'path.php';

if(isset($post['type'])&&$post['type']!=''){
	
	if($post['type']=='menu'){
		echo returnjson(['jsonmenu'=>$jsonmenu,'tempmenu'=>$tempmenu]);
	}
	
	if($post['type']=='select'){
		if(isset($post['jsonvalue'])){
			$filepath = $jsonpath.'/'.$post['jsonvalue'];
			$json = file_get_contents($filepath);
			$conarr = json_decode($json,true);
			echo returnjson($conarr);
		}
	}
	
	if($post['type']=='config'){
		$filepath = $jsonpath.'/'.$post['type'].'.json';

		unset($post['type']);
		$content = json_encode($post,JSON_UNESCAPED_UNICODE);
		
		$handle = fopen ($filepath,"w"); //打开文件指针，创建文件
		
		if (!is_writable ($filepath)){
		die ("文件：".$filepath."不可写，请检查其属性后重试!<br>");
		}
		
		if (!fwrite ($handle,$content)){ //将信息写入文件
		die ("文件".$filepath."更新失败!<br>");
		}
		
		fclose ($handle); //关闭指针
		
		echo ("文件".$filepath."更新成功!<br>");
		
	}
	else{
		$filepath = $jsonpath.'/'.$post['type'];
		unset($post['type']);
		
		$json = file_get_contents($filepath);
		$conarr = json_decode($json,true);
		//此处数据为空有错误
		if(count($conarr)==0){$conarr=[];}
		if($post['system']=='update'){
			unset($post['system']);
			foreach($conarr as $key => $value){
				if($post['id']==$value['id']){
					$conarr[$key] = $post;
				}
			}
		}
		else if($post['system']=='delete'){
			unset($post['system']);
			foreach($conarr as $key => $value){
				if($post['id']==$value['id']){
					unset($conarr[$key]);
				}
			}
		}
		else{
			unset($post['system']);
			foreach($conarr as $key => $value){
				if($post['id']==$value['id']){
					die('重复ID('.$post['id'].')新增失败!<br>');
				}
			}
			array_push($conarr,$post);
			
		}
		
		
		

		$content = json_encode($conarr,JSON_UNESCAPED_UNICODE);
		
		$handle = fopen ($filepath,"w"); //打开文件指针，创建文件
		
		if (!is_writable ($filepath)){
		die ("文件：".$filepath."不可写，请检查其属性后重试!<br>");
		}
		
		if (!fwrite ($handle,$content)){ //将信息写入文件
		die ("文件".$filepath."更新失败!<br>");
		}
		
		fclose ($handle); //关闭指针
		
		echo ("文件".$filepath."更新成功!<br>");
	}
	
}

die;

function returnjson($arr){
	return die(json_encode($arr));
}


?>