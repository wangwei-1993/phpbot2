<?php
/******************
***PHPBOT
***Author:wangwei
***Email:915335526@qq.com
***Versions:1.0
******************/
// 模板及静态路径
$GLOBALS['path'] = '../../../'; // 根目录
$GLOBALS['post'] = $_POST;

$jsonpath = $path.'json';
$tempath = $path.'template';
$htmlpath = $path.'html';

$tempname = isset($post['tempname'])?$post['tempname']:"default";
$GLOBALS['dirtpath'] = $tempath.'/'.$tempname;
$GLOBALS['dirhpath'] = $htmlpath.'/'.$tempname;
$limit = 6; //分页每页显示数量

$jsonmenu = [];
$tempmenu = [];
foreach(scandir($jsonpath) as $key => $value){
	if($key>1){
		array_push($jsonmenu,$value);
	}
}
foreach(scandir($tempath) as $ks => $vs){
	if($ks>1){
		array_push($tempmenu,$vs);
	}
}
$GLOBALS['jsonmenu'] = $jsonmenu;
$GLOBALS['tempmenu'] = $tempmenu;
?>