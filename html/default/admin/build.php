<?php
/******************
***PHPBOT
***Author:wangwei
***Email:915335526@qq.com
***Versions:1.0
******************/
header('Content-Type:text/html;charset=utf-8');//编码设置

include 'path.php';

function deletefile($dirhpath){  /*递归清空html*/
	if (is_dir($dirhpath)) {
		foreach(scandir($dirhpath) as $t =>$u){
			$htmlzipath = $dirhpath.'/'.$u;
			$fuji = explode("\\",dirname(__FILE__));
			if($u!=end($fuji)){
				if(!strpos($u,'.')){
					if($u!='.' && $u!='..'){
						deletefile($htmlzipath);
						if(count(scandir($htmlzipath)) == 2){
							rmdir($htmlzipath);
						}
					}
				}
				else{
					if(strpos($u,'.html')){
						unlink($htmlzipath);
					}
				}
			}
		}
	}
}
deletefile($dirhpath);

if (is_dir($dirtpath)) {
	$menuarr = [];
	foreach(scandir($dirtpath) as $key =>$value){
		if(strpos($value,'.html') !== false && $value!='footer.html' && $value!='header.html' && !preg_match("/[\x7f-\xff]/", $value)){
			array_push($menuarr,str_replace(strrchr($value, "."),"",$value));
		}
	}

}

//PHP生成静态html页面原理

function build($templatename,$id=null,$diy=[],$limit=0,$total=0){
	ob_start();
	$htmlname = $templatename;
	$limit = $limit;
	$total = $total;
	$nums = $id;
	$diyarr = $diy;
	$content = include($GLOBALS['path']."php/template.php"); 
	ob_get_clean(); 
	$wenjian = $GLOBALS['dirhpath'];
	
	if(strpos($templatename,'-')){
		$templatename = explode('-',$templatename)[0];
	}
	
	if (!file_exists($wenjian.'/'.$templatename) && $templatename!='index'){
	    mkdir($wenjian.'/'.$templatename,0777,true);
	}
	if($limit){
		$filename = $wenjian.'/'.$templatename."/index".($id?'-'.($id+1):'').".html"; //列表页路径
	}
	else{
		if($id){
			$filename = $wenjian.'/'.$templatename.'/'.$id.".html";  //详情页路径
		}
		else{
			$filename = $wenjian.'/'.$templatename."/index.html"; //单页路径
			if($templatename=='index'){
				$filename = $wenjian.'/'.$templatename.".html"; //首页路径
			}
		}
	}
	$handle = fopen ($filename,"w"); //打开文件指针，创建文件
	
	if (!is_writable ($filename)){
	die ("文件：".$filename."不可写，请检查其属性后重试!<br>");
	}
	
	if (!fwrite ($handle,$content)){ //将信息写入文件
	die ("文件".$filename."生成失败!<br>");
	}
	
	fclose ($handle); //关闭指针
	
	echo ("文件".$filename."生成成功!<br>");
}


for($i=0;$i<count($menuarr);$i++){
	
	$menuarr[$i] = strpos($menuarr[$i],'-')?$explode[0]:$menuarr[$i];

		
		$explode = explode('-',$menuarr[$i]);
		
		$filepath = $jsonpath.'/'.$menuarr[$i].'.json';
		
		if(!file_exists($filepath)){
			if(!is_dir($dirhpath.'/'.$menuarr[$i])){
				build($menuarr[$i]); /*生成单页*/
			}
		}
		else{
		
			$json = file_get_contents($filepath);
			$conarr = json_decode($json,true);
			$diy = [];
			foreach($conarr as $k => $v){
				$diyarr = [];
				foreach($v as $x => $z){
					$diyarr[$x]=$z;
				}
				array_push($diy,$diyarr);
				build($menuarr[$i].'-content',$conarr[$k]['id'],$conarr); /*生成详情页*/
			}
			$numpage = ceil(count($conarr)/$limit); 
			if($numpage>1){
				for($p=1;$p<$numpage;$p++){
					$listpath = $dirtpath.'/'.$menuarr[$i].'.html';
					if(!file_exists($listpath)){
						exit("文件".$listpath."不存在!<br>");
					}
					build($menuarr[$i],$p,$diy,$limit,count($conarr)); /*生成列表分页*/
					
				}
			}

			build($menuarr[$i],null,$diy,$limit,count($conarr)); /*生成列表首页*/
		
		}
		
	
	

}

?>