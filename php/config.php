<?php
/******************
***Author:wangwei
***Email:915335526@qq.com
***Versions:1.0
******************/
	function jsonarr($jsonname){
		$configpath = $GLOBALS['path']."json/".$jsonname.".json";
		
		$configjson = file_get_contents($configpath);
	
		return json_decode($configjson,true);
	}
	
	function handle($setting,$filestring,$diyarr,$limit=0,$total=0,$nums=0,$htmlname){
		
		$fors = '{{for';
		if(strpos($filestring,$fors)){
			if(strpos($filestring,'{{for}}')){
				$fors = '{{for}}';
			}
			
			$oldstr = get_between($filestring, $fors, '{{/for}}');
			
			if(!strpos($filestring,'{{for}}')){
				$canstr = get_between($oldstr, ' ', '}}');
				$strarr = ['"',"'"];
				foreach($strarr as $ak =>$av){
					$canstr = preg_replace ( "/\s(?=\s)/","\\1", str_replace($av,'',$canstr));
				}
				$subarr = explode(' ',$canstr);
				foreach($subarr as $bk =>$bv){
					if($bv){
						$can = explode('=',$bv);
						$$can[0] = $can[1];
					}
				}		
			}
			
			$limit = isset($number)?(isset($start)?$number+$start:$number):$limit;
			$jsonval = isset($model)?jsonarr($model):[];
			
			if(!strpos($filestring,'{{for}}')){
				if($limit > count($jsonval)){ $limit = count($jsonval); }
			}
			
			$ddd = $limit;
			if($total < ($nums+1)*$limit && $total){ $ddd = $total - $nums*$limit; }
			
			$newstring = '';
			
			if(!strpos($filestring,'{{for}}')){
				$one = $limit;
				$two = isset($start)?$start:0;
				$three = 0;
			}
			else{
				$one = $ddd;
				$two = 0;
				$three = $nums*$limit;
			}
				
			for($f=$two;$f<$one;$f++){
				$string = [];
				$variate = [];
				
				$sjarr = [];		
				if(getmaxdim($diyarr)!=1){ 
					$sjarr = $diyarr[$three+$f];
					$config = array_merge($setting,$sjarr);
				}
				else{
					//传模型取数据
					if(count($jsonval)){
						$sjarr = $jsonval[$three+$f];
					}
					$config = array_merge($setting,$sjarr);
				}
				
				foreach($config as $key => $value){
					array_push($string,'{{'.$key.'}}');
					
					$$key=$value;
					array_push($variate,$$key);
					
					foreach(get_baohan($filestring,'{{'.$key,'}}') as $kb =>$vb){
						if(!in_array($vb,$string)){
							array_push($string,$stc = $vb);
							$length = get_danci($stc);
							array_push($variate,strlen($$key)>$length?($length<0?substr($$key,$length):substr($$key,0,$length)):$$key);
						}
					}

				}
				
				$htmlcon = str_replace($string,$variate, $filestring);
				$substring = get_between($htmlcon, '}}', '{{/for}}');
				$newstring .= $substring;
				
			}

			$htmlstring = str_replace($fors.$oldstr.'{{/for}}',$newstring, $filestring);

			if(strpos($filestring,$fors)){
				return handle($setting,$htmlstring,$diyarr,$limit,$total,$nums,$htmlname);
			}
			
		}
		else{
			// 详情页标签替换
			$string = [];
			$variate = [];
			$shangxia = [];
			if(getmaxdim($diyarr)==2){ //上下篇
				if($nums==1){
					$shangxia['pre'] = '<a>没有了</a>';
					$shangxia['next'] = '<a href="'.$diyarr[$nums]['url'].'">'.$diyarr[$nums]['tit'].'</a>';
				}
				if($nums==count($diyarr)){
					$shangxia['pre'] = '<a href="'.$diyarr[$nums-2]['url'].'">'.$diyarr[$nums-2]['tit'].'</a>';
					$shangxia['next'] = '<a>没有了</a>';
				}
				if($nums>1&&$nums<count($diyarr)){
					$shangxia['pre'] = '<a href="'.$diyarr[$nums-2]['url'].'">'.$diyarr[$nums-2]['tit'].'</a>';
					$shangxia['next'] = '<a href="'.$diyarr[$nums]['url'].'">'.$diyarr[$nums]['tit'].'</a>';
				}
				
				$diyarr = $nums>0?$diyarr[$nums-1]:[];
				$diyarr = array_merge($diyarr,$shangxia);
				
			}
			$config = array_merge($setting,$diyarr);
			
			if(strpos($filestring,'{{page}}')){ //上下页
				$stpr = '';
				if($nums==1){
					$stpr .= '<a href="/'.$htmlname.'">上一页</a>';
				}
				else if($nums>0){
					$stpr .= '<a href="index-'.$nums.'.html">上一页</a>';
				}
				else{
					$stpr .= '<a>上一页</a>';
				}
				
				for($pi=0;$pi<ceil($total/$limit);$pi++){
					//替换参数标签操作					
					$stpr .= '<a class="'.($pi==$nums?'page-num-current':'page-num').'" href="'.($pi?'index-'.($pi+1).'.html"':'/'.$htmlname.'"').'>'.($pi+1).'</a>';
				}
				
				if($nums<ceil($total/$limit)-1){
					$stpr .= '<a href="index-'.($nums+2).'.html">下一页</a>';
				}
				else{
					$stpr .= '<a>下一页</a>';
				}
				
				$stpr .= '<a>'.($nums+1).'/'.ceil($total/$limit).'</a>';
				
				$fenye['page'] = $stpr;
				$config = array_merge($config,$fenye);
			}

			foreach($config as $key => $value){
				array_push($string,'{{'.$key.'}}');

				$$key=$value;
				array_push($variate,$$key);
				
				foreach(get_baohan($filestring,'{{'.$key,'}}') as $kb =>$vb){
					if(!in_array($vb,$string)){
						array_push($string,$stc = $vb);
						$length = get_danci($stc);
						array_push($variate,strlen($$key)>$length?substr($$key,0,$length):$$key);
					}
				}
			}
			$htmlstring = str_replace($string,$variate, $filestring);
			
		}
		
		return $htmlstring;
		
	}
	
	function getmaxdim($vDim){
	  if(!is_array($vDim)) return 0;
	  else
	  {
	    $max1 = 0;
	    foreach($vDim as $item1)
	    {
	     $t1 = getmaxdim($item1);
	     if( $t1 > $max1) $max1 = $t1;
	    }
	    return $max1 + 1;
	  }
	}
	
	function get_between($t, $s, $e) {
	 $sbt = substr($t, strlen($s)+strpos($t, $s),(strlen($t) - strpos($t, $e))*(-1));
	 return $sbt;
	}
	
	function get_baohan($t, $s, $e) {
		$userinfo = "/".$s."(.*)".$e."/U";
		preg_match_all ($userinfo, $t, $pat_array);
		return $pat_array[0];
	}
	
	function get_danci($str) {
		$userinfo = "/(\'|\")(.*)(\'|\")/i";
		preg_match ($userinfo, $str, $pat_array);
		return trim(trim($pat_array[0],'"'),"'");
	}
?>